Instructions
=============

- First step
-------------
Clone project to folder 
###### git clone https://PiotroSan@bitbucket.org/PiotroSan/allegro-zadanie.git

- Second step
-------------
Install requirements
###### pip install -r requirements.txt

- Third step
-------------
Run server
###### python ./manage.pu runserver

- Fourth step
-------------
Run Postman

- Fifth step
-------------
Send request eq.  
http://127.0.0.1:8000/repository/{owner}/{project}

- Sixth step
-------------
Run test in root project folder
###### python -m unittest api.test_api