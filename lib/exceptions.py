
class Response404Exception(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, 'Repository doesn\'t exist')


class Response401Exception(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, 'You are not authenticated')