#!/usr/bin/env python
# -*- coding: utf-8 -*-

import appapi.settings as settings
import requests
import base64

from lib.exceptions import Response404Exception, Response401Exception

"""
Module git_api.py

Moduł zawiera pomocnicze klasy, biblioteki 
wykorzystywane do pracy z GITHUBEM 
"""


class Requester(object):
    """
    Klasa odpwowiada za przygotowanie parametrów
    dla zapytań do api GitHuba
    """

    def __init__(self, login='', password=''):
        self.credential_header = \
            "Basic " + base64.b64encode(
                (login + ":" + password).encode("utf-8")
            ).decode("utf-8").replace('\n', '')

    def set_header(self):
        """
        Metoda zwraca nagłówek zapytaia HTTP,
        w obecnej chwili jest uzupełniany tylko o dane autoryzacyjne
        :return:
        """
        self.header = {'Authorization': self.credential_header}

    def validate_response(self, response):
        if response.status_code == 404:
            raise Response404Exception()

        if response.status_code == 401:
            raise Response401Exception()


class Respository(object):
    """
    Klasa Repozytorium jest reprezentacją
    obiektu Repozytorium istenijącego w GitHubie.
    """
    GET_URL = '/repos/{owner}/{repo}'

    def __init__(self, base_url, requester):
        self.requester = requester
        self.base_url = base_url

    def get(self, **params):
        """
        Metoda get odpowiada za pobranie danych dotyczących repozytorium
        z GitHUba
        Parametrami jest owner oraz repo dostarczane w adresie url
        zapytania kierowanego do tej aplikacji.
        Dalej działa jak proxy i przekierowuje zapytanie do api GitHuba.

        :param params:
        :return:
        """
        url = self.GET_URL.format(
            owner=params.get('owner', ''),
            repo=params.get('repo', ''),
        )
        self.requester.set_header()
        response = requests.get(self.base_url + url, headers=self.requester.header)
        self.requester.validate_response(response)
        return response


class GitApi(object):
    """
    Klasa podstawowa, fasada dla obsługi zapytań do
    GitHuba. Zbudowana na zasadzie kompozycji.
    Pierwszy element dotyczy repozytorium.
    """

    def __init__(self, requester, base_url=None):
        self.repository = Respository(base_url or settings.GIT_BASE_URL, requester)
