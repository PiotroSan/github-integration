from rest_framework import serializers
from dateutil import parser


class GitDataSerializer(serializers.Serializer):
    fullName = serializers.CharField(source='full_name')
    description = serializers.CharField()
    cloneUrl = serializers.URLField(source='clone_url')
    createdAt = serializers.SerializerMethodField()
    stars = serializers.IntegerField(source='stargazers_count')

    def get_createdAt(self, value):
        create_at = value.get('created_at', '')
        if not create_at:
            return create_at
        return parser.parse(create_at).strftime('%Y-%M-%d')
