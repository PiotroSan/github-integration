import requests
import appapi.settings as settings

from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer

from lib.git_api import (GitApi, Requester)
from lib.exceptions import Response404Exception, Response401Exception

from api.serializers import GitDataSerializer

"""
Module viewsets.py
Klasa zawierająca metodę list obsługująca zapytania GET.

"""


class GitViewSet(viewsets.ViewSet):
    """
        Metoda list przyjmuje paramtery owner oraz repositoryname
        przekazywane w adresie url.
        Wzorzec adresu url:
        'repository/(?P<owner>\w+)/(?P<repositoryname>\w+)'
    """
    permission_classes = [AllowAny]

    def list(self, request, owner, repositoryname):
        """
        Endpoint wbudowana metoda DajgResponseDatao REST FRAMEWORK
        pozwala na nie umieszczanie jej nazwy w zapytaniach http.
        Obsługuje zapytania GET.

        :param request:
        :param owner:
        :param repositoryname:
        :return:
        """
        data = dict()

        params = {
            'owner': owner,
            'repo': repositoryname,
        }

        request_parameters = Requester(settings.GIT_USER, settings.GIT_PASSWORD)
        git_api = GitApi(request_parameters)

        try:
            response = git_api.repository.get(**params)
            serializer = GitDataSerializer(response.json())
            data = serializer.data
        except Response404Exception as e:
            data.update({'message': str(e)})
        except Response401Exception as e:
            data.update({'message': str(e)})
        except requests.exceptions.ConnectionError as e:
            data.update({'message': 'Server unavaible'})
        except TypeError as e:
            data.update({'message': str(e)})
        finally:
            return Response(JSONRenderer().render(data))


