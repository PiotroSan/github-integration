#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import unittest

from api.serializers import GitDataSerializer
from lib.git_api import GitApi, Requester
from lib.exceptions import Response401Exception, Response404Exception


def check_response_data(data):
    return data or False


class TestGitIntegration(unittest.TestCase):
    """
    This is a test class, this is integration test.
    I used unit test to build integration test.
    """

    def setUp(self):
        self.request_parameters = Requester()
        self.params = {
            'owner': 'piotrosan',
            'repo': 'weekplan',
        }

    def runTest(self):
        """
        This function run integration test.
        We check the response data is empty.
        :return:
        """
        data = dict()

        git_api = GitApi(self.request_parameters)

        try:
            response = git_api.repository.get(**self.params)
            serializer = GitDataSerializer(response.json())
            data = serializer.data
        except Response404Exception as e:
            pass
        except Response401Exception as e:
            pass
        except requests.exceptions.ConnectionError as e:
            pass
        except TypeError as e:
            pass

        assert check_response_data(data)
